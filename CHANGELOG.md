# Compute Unit Changelog

## 0.5.1
Released 12/30/2020

* Add cpu uuid and bios methods
* Remove formatters module
* Fix stack level when root? is used
* Ensure cpu power is an integer
## 0.5.0
Released 10/5/2020

* Add more method docs
* Allow overriding of cpu power
* Update cpu name using model
* Add new search methods
* Add voltage and other cpu metrics
## 0.4.0
* Adds ability to list attached processes to compute_unit sorted by field
* Adds ability to list top_x attached processes sorted by field

## 0.3.3
* Fix bug with default database not being created

## 0.3.2
* Fix bug with default database not being created

## 0.3.1
Released 9/24/2020

* Use the default system pcidb file instead of throwing error
* Return default data if no hmon value exist

## 0.3.0
Released 6/16/2020

* Add name method to the cpu class
* Use constant for proc path
* Use debug when kernel file doesn't exist

## 0.2.1
Released 6/6/2020

* update rake and other gems

## 0.2.0
Released 6/5/2020
* Added more cpu methods for various POI.
* Ensure constants exist when side loaded
* Add descendent discovery mechanism to GPU class

## initial version 0.1.0