# Compute Unit Ruby Library
The compute unit library helps you write future abstractions and tooling for crypto mining, machine learning or just general purpose.  This library was extracted out of the crypto mining tool [Crossbelt](https://crossbelt.blockops.party) to help spur more tooling for ruby. 

Uses the sysfs filesystem to quickly read and write compute unit data. More information about sysfs can be found [here](https://www.kernel.org/doc/Documentation/filesystems/sysfs-pci.txt)

This library has the ability to:

 * Detect all compute devices such as cpu, gpu, and anything else we add to this library. 
 * Provide the ability to control GPU fans.
 * Get temperature of units.
 * Change or view voltage, core clock, memory clock and other tunning parameters used for overclocking.
 * Provide inventory data of all the devices with make, model and other important information. 
 * Provide realtime status information of various data points. Useful for real-time tunning. 
 * Interact with the compute device via ruby.

This library was open sourced to allow further refinement and usage throughout the ruby community. 
Since machine learning is not popular on ruby due to device and program support, I hope this gem will 
make ML and crypto more viable for ruby.  

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'compute_unit'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install compute_unit

## Usage

Ensure the pci database exist on your system.  You can update it at anytime with the built in linux command.

`/usr/sbin/update-pciids` or the command from this gem `update_pcidb` installed in your gem bin directory.

Additionally, when using the `ComputeUnit.find_all_with_database` method.

Find all compute devices
```
require 'compute_unit'
devices = ComputeUnit.find_all

```

Find just GPU devices and list the voltage table

```ruby
require 'compute_unit/gpu'

gpu = ComputeUnit::Gpu.find_all.first
gpu.voltage_table
=> [{:pstate=>0, :clk=>852, :volt=>800, :type=>:sclk},
 {:pstate=>1, :clk=>997, :volt=>860, :type=>:sclk},
 {:pstate=>2, :clk=>1084, :volt=>860, :type=>:sclk},
 {:pstate=>3, :clk=>1138, :volt=>860, :type=>:sclk},
 {:pstate=>4, :clk=>1200, :volt=>860, :type=>:sclk},
 {:pstate=>5, :clk=>1401, :volt=>860, :type=>:sclk},
 {:pstate=>6, :clk=>1536, :volt=>860, :type=>:sclk},
 {:pstate=>7, :clk=>1630, :volt=>1200, :type=>:sclk}]

gpu.amdgpu_pm_info
=> {:mclk=>{:value=>"1020", :unit=>"MHz"},
 :sclk=>{:value=>"994", :unit=>"MHz"},
 :pstate_sclk=>{:value=>"1138", :unit=>"MHz"},
 :pstate_mclk=>{:value=>"800", :unit=>"MHz"},
 :vddgfx=>{:value=>"862", :unit=>"mV"},
 :average_gpu=>{:value=>"121.0", :unit=>"W"},
 :temperature=>{:value=>"57", :unit=>"C"},
 :load=>{:value=>"100", :unit=>"%"}} 

```

## Example CLI command
There is a `list_computes` command that will enumerate all the compute devices found on the system.

```ruby
list_computes
[
  {
    "uuid": "GPU0",
    "gpuId": "GPU0",
    "syspath": "/sys/bus/pci/devices/0000:03:00.0",
    "pciLoc": "0000:03:00.0",
    "name": "RX Vega56",
    "bios": "113-D0500100-102",
    "subType": "amdgpu",
    "make": "AMD",
    "model": "RX Vega56",
    "vendor": "AMD",
    "power": 121,
    "utilization": 100,
    "temperature": 57,
    "status": 0,
    "pstate": -1,
    "fanSpeed": 2946,
    "type": "GPU",
    "maxTemp": null,
    "mem": 1020,
    "cor": 997,
    "vlt": 862,
    "mem_temp": 75,
    "maxFan": null,
    "dpm": null,
    "vddci": null,
    "maxPower": null,
    "ocProfile": null,
    "opencl_enabled": false
  },
  {
    "chip_make": "GenuineIntel",
    "make": "GenuineIntel",
    "model": "Intel(R) Core(TM) i3-7100 CPU @ 3.90GHz",
    "device_id": "590f",
    "vendor_id": "8086",
    "subsystem_device_id": "7a59",
    "subsystem_vendor_id": "1462",
    "device_class": "060000",
    "temp": 33,
    "minFreqMhz": 800,
    "maxFreqMhz": 3900,
    "currentFreqMhz": 3600,
    "numCores": 2,
    "numThreads": 2,
    "nproc": 4,
    "temps": {
      "package_id_0": 33,
      "core_0": 26,
      "core_1": 33
    }
  }
]
```

### Subclassing a compute object
Instead of monkey patching this code you can simply subclass the objects.  The find_all methods are intelligent enough to locate your subclass by searching the Objectspace for all decendants of `ComputeUnit::ComputeBase`.  This makes it dead simple to add new functionality without having to wrap any code.  This gives you the ability to include new modules or override existing methods.  Just make sure you load your class before using the find_all method.



```ruby
# coolproject/vegagpu.rb
require 'compute_unit/gpu/amdgpu'
module CoolProject
  class VegaGpu < ::ComputeUnit::Gpu::AmdGpu
    include CoolProject::Metrics

    def initialize(device_path, opts)
        super(device_path, opts)
        @is_vega = true
    end
  end
end

```

Once you have your new class created all you need to do is load the class before running find all.

```ruby
require 'compute_unit'
require 'coolproject/vegagpu'
require 'yaml'

puts ComputeUnit.find_all.to_yaml

```

## Make and Model information
This library uses the pci database maintained here https://pci-ids.ucw.cz/ which is also used by `lspci` and other linux tools. If your device is giving the wrong info the problem is likely due to the database being incorrect. 

You can also manually refresh the pci database file programmatically via `ComputeUnit.refresh_pci_database`

If you want to refresh the database every time with finding all the devices you can use: `ComputeUnit.find_all_with_database`

Or you can also run the simple cli tool. `update_pcidb` that comes with this library. 

## Kernel support
There will likely be changes with reading and writing information to sysfs as new kernels are released.  This library has been tested against. 

* 4.15 - 4.20
* 5.x 

## OpenCL Usage
The opencl_ruby_ffi gem is optionally used to gather additional info about the GPU.  This can sometimes result in better model info.  

There is some caution when using OpenCL.  If a GPU is dead, OpenCL tends to hang/freeze thus forcing a reboot.  This is not a bug in the compute_unit or opencl_ruby_ffi gem but in the OpenCL library instead.

Getting data from OpenCL also takes a long time!  So if you have multiple GPUs in your system this could takes multiple seconds to return data.  Because of this we cache the responses for the next query.  This response is cached until the system checksum changes.  This checksum is a special calculatulation of all the devices present in the system.  So if you ever added or changed the equipment in the system, a new OpenCL query would ensue. 

To enable OpenCL you just need to pass a boolean value to any of the find_all methods:

```ruby
require 'compute_unit'
devices = ComputeUnit.find_all(true)

```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/blockops/compute_unit. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT.

## Code of Conduct

Everyone interacting in the ComputeUnit project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/blockops/compute_unit/-/blob/master/CODE_OF_CONDUCT.md).
