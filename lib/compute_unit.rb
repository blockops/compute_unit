# frozen_string_literal: true

require 'compute_unit/utils'
require 'compute_unit/version'
require 'compute_unit/exceptions'
require 'compute_unit/monkey_patches'

module ComputeUnit
  CACHE_DIR = File.join('/var', 'run', 'compute_unit_cache')
  SYSFS_PATH = ENV['SYSFS_PATH'] || '/sys'
  SYS_DEVICE_PATH = File.join(SYSFS_PATH, 'bus/pci/devices')
  PCI_DATABASE_PATH = File.join(File.dirname(__dir__), 'pci.ids')
  PCI_DATABASE_URL = 'http://pci-ids.ucw.cz/v2.2/pci.ids'
  DEFAULT_PCIDB_PATH = '/usr/share/misc/pci.ids'

  # @param use_opencl [Boolean]
  # @return [Array] - return a list of compute units
  def self.find_all(use_opencl = false)
    require 'compute_unit/gpu'
    require 'compute_unit/cpu'
    require 'compute_unit/asic'

    # if this file doesn't exist we need to fetch it or copy it
    refresh_pci_database unless File.exist?(PCI_DATABASE_PATH)

    Gpu.find_all(use_opencl) + Cpu.find_all + Asic.find_all
  end

  # @param pid [Integer] the pid to search with
  # @param field [Symbol] - the field to sort by
  # @return [Array] - a array of device paths
  # @example usage
  #   device_paths_by_process(3748) => ["/sys/bus/pci/devices/0000:00:00.0"]
  def self.device_paths_by_process(pid)
    # processes = ComputeUnit::Cpu.attached_processes(field).last(1) + ComputeUnit::Gpu.attached_processes(field)
    # processes.find_all { |process| process.pid == pid }
    # We can get the utilized devices but it appears cubersome to convert to a device path
    # This method uses more resources
    find_by_process(pid).map(&:device_path)
  end

  # @param pid [Integer] the pid to search with
  # @param use_opencl [Boolean] use opencl on gpu devices
  def self.find_by_process(pid, use_opencl = false)
    find_all(use_opencl).find_all do |unit|
      unit.top_processes.first.pid.to_i == pid.to_i
    end
  end

  # copies the default pci database from linux filesystem over to the gem path
  def self.copy_default_database
    FileUtils.cp(DEFAULT_PCIDB_PATH, PCI_DATABASE_PATH) if File.exist?(DEFAULT_PCIDB_PATH)
  end

  # get a fresh copy of the database and then use find_all
  # @param use_opencl [Boolean]
  # @return [Array] - return a list of compute units
  def self.find_all_with_database(use_opencl = false)
    refresh_pci_database
    find_all(use_opencl)
  end

  # downloads the pci database
  def self.refresh_pci_database
    ComputeUnit::Utils.check_for_root
    require 'net/http'
    require 'uri'
    uri = URI.parse(PCI_DATABASE_URL)
    response = Net::HTTP.get_response(uri)
    # I can't write to it unless it has correct permissions
    File.chmod(0o644, PCI_DATABASE_PATH) if File.exist?(PCI_DATABASE_PATH)
    File.write(PCI_DATABASE_PATH, response.body) if response.code == '200'
    File.chmod(0o644, PCI_DATABASE_PATH)
    PCI_DATABASE_PATH
  end
end
