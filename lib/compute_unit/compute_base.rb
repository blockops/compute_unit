# frozen_string_literal: true

require 'time'
require 'compute_unit/logger'
require 'compute_unit/device'
require 'sys/proctable'

module ComputeUnit
  class ComputeBase < Device
    include Sys

    attr_reader :type, :serial, :meta, :uuid, :timestamp, :index, :compute_type
    attr_accessor :power_offset

    include ComputeUnit::Logger

    # timeout value
    CACHE_TIMEOUT = 30

    # @return [Array] - find all the decendants of thyself
    def self.compute_classes
      ObjectSpace.each_object(Class).select do |klass|
        # <Class:#<Crossbelt::ComputeUnit::NvidiaGpu:0x00007fddc5c02a10>>
        #  We have to filter out these kinds of Ojbects as they don't respond to :new
        #  without a singleton error.
        klass < self && !klass.to_s.include?('Class')
      end
    end

    # @summary Finds all cpu attached processes and sorts by pctcpu
    # param filter [Regex] - if supplied filter out devices from fd list
    # @param field [Symbol] - the field to sort by
    # @return [Array] - an array of attached processes
    def attached_processes(field = :pctcpu, filter = nil)
      raise NotImplementedError unless self.class.respond_to?(:attached_processes)

      self.class.attached_processes(field, filter)
    end

    # @summary Find the processes consuming the most cpu
    # @param x [Integer] the number of processes to return, defaults to 1
    # @param field [Symbol] - the field to sort by
    # @return [Array] - an array of attached processes
    def top_processes(x = 1, field = :pctcpu)
      attached_processes(field).last(x)
    end

    # @param value [Float] a value to offset the power calculation, either a whole number, or decimal
    # @return [Integer] the value set as the offset
    def power_offset=(value)
      @power_offset = value.is_a?(Float) && value.abs < 1 ? (value * power).round(0) : value.to_i
      # Power offset by #{@power_offset} watts for #{compute_type}#{index},
      # future calculations will include this offset")
    end

    def initialize(device_path, opts = {})
      super
      @timestamp = Time.now.to_i
    end

    def device_class_name
      self.class.const_get('DEVICE_CLASS_NAME')
    end

    # @return [Array] - an array of pci bus device locations (every device on the pci bus)
    # @note there is not a filter applied
    def self.devices
      Dir.glob(File.join(ComputeUnit::Device::SYSFS_DEVICES_PATH, '*'))
    end

    def expired_metadata?
      (timestamp + CACHE_TIMEOUT) < Time.now.to_i
    end
  end
end
