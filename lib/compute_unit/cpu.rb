# frozen_string_literal: true

require 'compute_unit/compute_base'

module ComputeUnit
  class Cpu < ComputeBase
    DEVICE_CLASS = '060000'
    DEVICE_CLASS_NAME = 'CPU'
    VOLTAGE_MSR = 0x198
    # @return [Array] - returns a list of device paths of all devices considered for display

    def self.devices
      ComputeUnit::ComputeBase.devices.find_all do |device|
        ComputeUnit::Device.device_class(device) == DEVICE_CLASS
      end
    end

    def bios
      'N/A'
    end

    def utilization
      1 # until we can calculate this.  Is loadavg a good metric? or load / cpus
    end

    # @summary Finds all cpu attached processes and sorts by pctcpu
    # @param field [Symbol] - the field to sort by
    # @param filter [Regex] - if supplied filter out devices from fd list
    # @return [Array] - an array of attached processes
    def self.attached_processes(field = :pctcpu, _filter = nil)
      Sys::ProcTable.ps(smaps: false).sort_by(&field)
    end

    # @summary [Float] - returns the voltage of the cpu
    # @param processor_id [Integer] - the id of the cpu
    def voltage(processor_id = 0)
      file = "/dev/cpu/#{processor_id}/msr"
      return 0 unless File.exist?(file)

      # read 8 bytes, then unpack it by turning it into an integer
      # make it a binary string, pluck out some specific bits, then
      # convert it back to an integer
      # divide by 8192 to give the voltage
      # lowbit = 32
      # highbit = 47
      # bits = 47 - 32 + 1 # we want to read bits 32-47
      msr = IO.new IO.sysopen(file, 'rb')
      msr.sysseek(VOLTAGE_MSR)
      data, = msr.sysread(8).unpack('q')
      format('%.2f', ((data >> 32) / 8192.to_f))
    end

    # @return [String] - the model / name of the cpu
    def model
      raw_cpu_data[:model_name]
    end

    # @return [String] - the model / name of the cpu
    def name
      model
    end

    # @return [String] - the maker of the cpu
    def make
      raw_cpu_data[:vendor_id]
    end

    # @return [Integer] - the number of cores
    def num_cores
      raw_cpu_data[:cores_per_socket].to_i
    end

    # @return [Integer] - the number of threads
    def num_threads
      raw_cpu_data[:threads_per_core].to_i
    end

    # @return [Integer] - the number of cpus
    def nproc
      raw_cpu_data[:cpus].to_i
    end

    # @return [Float] - current mhz of cpu
    def current_freq_mhz
      raw_cpu_data[:cpu_mhz].to_f.round(0)
    end

    # @return [Float] - max mhz of cpu
    def max_freq_mhz
      raw_cpu_data[:cpu_max_mhz].to_f.round(0)
    end

    # @return [Float] - current min of cpu
    def min_freq_mhz
      raw_cpu_data[:cpu_min_mhz].to_f.round(0)
    end

    # @return [String] the path of the hwmon path for monitoring temps
    def base_hwmon_path
      File.join(ComputeUnit::SYSFS_PATH, 'devices/platform/coretemp.0/hwmon')
    end

    # @return [Hash] - a hash of temp readings and their labels
    # @example temps => {:core_0=>31, :core_1=>31, :package_id_0=>27}
    def temps
      Dir.glob(File.join(hwmon_path, 'temp*_label')).each_with_object({}) do |label_file, acc|
        temp_file = label_file.sub('label', 'input')
        label = normalize_name(read_file(label_file))
        reading = read_file(temp_file).to_f.round(0) / 1000
        acc[label] = reading
        acc
      end
    end

    # @return [Integer] - the temperature of the cpu package in Celsius
    def temp
      read_hwmon_data('temp1_input', 0).to_f.round(0) / 1000
    end

    def status
      0
    end

    def power
      ENV.fetch('CPU_POWER', 60).to_i # until we can calculate power we will just use 60
    end

    def fan
      2500 # until we can calculate fan speed
    end

    def mem_temp
      0 # until we can get the mem temp
    end

    def pci_loc
      device_path
    end

    def status_info
      { index: uuid,
        name: model,
        bios: 'N/A',
        core_clock: current_freq_mhz,
        memory_clock: 'N/A',
        power: power,
        fan: fan,
        core_volt: voltage,
        temp: temp,
        mem_temp: mem_temp,
        status: status }
    end

    def metrics
      {
        uuid: uuid,
        temp: temp,
        minFreqMhz: min_freq_mhz,
        maxFreqMhz: max_freq_mhz,
        currentFreqMhz: current_freq_mhz,
        model: model,
        make: make,
        numCores: num_cores,
        numThreads: num_threads,
        nproc: nproc,
        temps: temps
      }
    end

    def to_h
      super.merge(metrics)
    end

    def initialize(_device_path, opts)
      super
      @type = :CPU
      @pci_loc = device_path
      @index = opts[:index].to_i
      @power_offset = 0
      @uuid = opts[:uuid] || opts[:serial]
    end

    # @return [String] - the type and index of the device
    def uuid
      @uuid ||= "#{type}#{index}"
    end

    def self.create_from_path(device_path, index)
      opts = {
        device_class_id: device_class(device_path),
        device_id: device(device_path),
        device_vendor_id: device_vendor(device_path),
        subsystem_vendor_id: subsystem_vendor(device_path),
        subsystem_device_id: subsystem_device(device_path),
        index: index
      }
      new(device_path, opts)
    end

    def self.find_all(_use_opencl = false)
      devices.sort.map.with_index do |device_path, index|
        create_from_path(device_path, index)
      end
    end

    private

    # @param value [String]
    # @return [String]
    # @summary downcases and transforms spaces into underscores
    def normalize_name(value)
      value.downcase.gsub(/\s/, '_').gsub('(s)', 's').to_sym
    end

    # @return [Hash] - a hash of cpu info returned from lscpu
    # normalizes the key name by downcasing and removing spaces
    # @example
    #   raw_cpu_data() =>
    # {"architecture"=>"x86_64", "cpu_op-modes"=>"32-bit, 64-bit", "byte_order"=>"Little Endian",
    #   "cpus"=>"4", "on-line_cpus list"=>"0-3", "threads_per core"=>"2", "cores_per socket"=>"2",
    #   "sockets"=>"1", "numa_nodes"=>"1", "vendor_id"=>"GenuineIntel", "cpu_family"=>"6",
    #   "model"=>"158", "model_name"=>"Intel(R) Core(TM) i3-7100 CPU @ 3.90GHz", "stepping"=>"9",
    #   "cpu_mhz"=>"800.545", "cpu_max mhz"=>"3900.0000", "cpu_min mhz"=>"800.0000",
    #   "bogomips"=>"7799.87", "virtualization"=>"VT-x", "l1d_cache"=>"32K", "l1i_cache"=>"32K",
    #   "l2_cache"=>"256K", "l3_cache"=>"3072K", "numa_node0 cpus"=>"0-3",
    #   "flags"=>"fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36
    #    clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc
    #   art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf pni
    #   pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1
    #   sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm
    #   3dnowprefetch cpuid_fault invpcid_single tpr_shadow vnmi flexpriority ept vpid ept_ad
    #   fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushopt intel_pt
    #   xsaveopt xsavec xgetbv1 xsaves dtherm arat pln pts hwp hwp_notify hwp_act_window hwp_epp"
    # }
    def raw_cpu_data
      @raw_cpu_data ||= begin
        # turns each line into a hash of key value pairs
        regex = /(?<name>[\s\w\-\(\)]+)\:\s+(?<value>[@\s\w\-\,\.\(\)]+)$/
        `lscpu`.lines.each_with_object({}) do |line, acc|
          m = line.match(regex)
          name = normalize_name(m.named_captures['name'])
          acc[name] = m.named_captures['value'].chomp if m
          acc
        end
      end
    end
  end
end
