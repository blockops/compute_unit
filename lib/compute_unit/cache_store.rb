# frozen_string_literal: true

require 'compute_unit/logger'
require 'fileutils'
require 'yaml/store'

module ComputeUnit
  class CacheStore
    attr_accessor :name
    include ComputeUnit::Logger

    def initialize(name)
      @name = name
      FileUtils.mkdir_p(File.dirname(cache_file))
      FileUtils.mkdir_p(cache_files_dir)
    end

    def cache_files_dir
      File.join(File.dirname(cache_file), 'files')
    end

    # @return [YAML::Store]
    def cache_store
      @cache_store ||= ::YAML::Store.new(cache_file)
    end

    # @return [String] - the file where the cache is stored, different per user, due to permissions issue
    def cache_file
      if Etc.getpwuid.name == 'root'
        File.expand_path(File.join(ComputeUnit::CACHE_DIR, name, "#{name}.db"))
      else
        File.expand_path(File.join('~', '.compute_unit-cache', name, "#{name}.db"))
      end
    end

    # @return [Object] - returns the stored object given the key, defaults to nil when not found unless specified
    # @param key [String] - the key to retrieve
    # @param default [Object] - the default value if no data is found
    def read_cache(key, default = nil)
      return default unless key

      begin
        cache_store.transaction do
          cache_store.fetch(key, default)
        end
      rescue StandardError => e
        logger.debug("Error reading from cache with key #{key}: #{e.message}")
        default
      end
    end

    # @return [?] -
    # @param key [String] - the key to retrieve
    # param value [Object] - the object to store in the cache
    def write_cache(key, value)
      return if ENV['XB_DISABLE_CACHE'] =~ /yes|true/i

      begin
        cache_store.transaction do
          cache_store[key] = value
        end
      rescue StandardError => e
        logger.debug("Error writing to cache with key #{key}: #{e.message}")
      end
    end

    def memory_cache
      @memory_cache ||= {}
    end

    def write_memcache(key, value)
      logger.debug("Caching content with key : #{key}")
      memory_cache[key] = {
        content: value,
        ttl: Time.now.to_i
      }
      value
    end

    def memory_cache_lookup(key, ttl = 300)
      return unless ttl.positive?

      logger.debug("Looking up memory cache for #{key}")
      hexkey = Digest::SHA1.hexdigest(key)
      cached_item = memory_cache[hexkey]
      return nil unless cached_item && cached_item[:content]

      timestamp = cached_item[:ttl] + ttl
      expired = Time.now.to_i
      if timestamp > expired
        logger.debug("Memory cache hit #{hexkey}")
        begin
          cached_item[:content]
        rescue JSON::ParserError
          nil
        end
      else
        logger.debug("Cleaning cache for #{hexkey}")
        memory_cache.delete(hexkey)
        nil
      end
    end

    # @return [Object] - return the parsed json object or nil only if the item ttl has not expired
    # @param key [String] - the url of request
    # @param ttl [Integer] - time in seconds to keep the request for
    def read_json_cache_file(key, ttl = 300)
      logger.debug("Looking up cache for #{key}")
      hexkey = Digest::SHA1.hexdigest(key)
      cache_file = File.join(cache_files_dir, "#{hexkey}.json")
      return nil unless cache_file
      return nil unless File.exist?(cache_file)

      timestamp = File.mtime(cache_file).to_i + ttl
      expired = Time.now.to_i
      if timestamp > expired
        logger.debug("Cache hit #{hexkey}")
        begin
          JSON.parse(File.read(cache_file))
        rescue JSON::ParserError
          nil
        end
      else
        logger.debug("Cleaning cache for #{hexkey}")
        FileUtils.rm(cache_file)
        nil
      end
    end

    # @return [?] -
    # @param key [String] - the key to retrieve
    # @param value [Object] - the object to store in the cache in json format
    def write_json_cache_file(key, value)
      return if ENV['XB_DISABLE_CACHE'] =~ /yes|true/i

      logger.debug("Caching content with key : #{key}")
      cache_file = File.join(cache_files_dir, "#{key}.json")
      File.open(cache_file, 'w') do |f|
        f.write(value)
      end
    end
  end
end
