# frozen_string_literal: true

require 'compute_unit/compute_base'
module ComputeUnit
  class Asic < ComputeBase
    DEVICE_CLASS_NAME = 'Asic'

    def self.find_all
      []
    end

    def self.create_from_path(device_path, index)
      opts = {
        device_class_id: device_class(device_path),
        device_id: device(device_path),
        device_vendor_id: device_vendor(device_path),
        subsystem_vendor_id: subsystem_vendor(device_path),
        subsystem_device_id: subsystem_device(device_path),
        index: index
      }
      new(device_path, opts)
    end

    def self.find_all
      return []
      devices.sort.map.with_index do |device_path, index|
        create_from_path(device_path, index)
      end
    end

    # /sys/devices/platform $ more coretemp.0/hwmon/hwmon1/temp1_input / 1000
  end
end
