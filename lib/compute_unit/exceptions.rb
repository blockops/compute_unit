# frozen_string_literal: true

module ComputeUnit
  module Exceptions
    class NoPermission < RuntimeError; end
    class PermissionDenied < RuntimeError; end
    class UnsupportedGPU < RuntimeError; end
    class NotSupported < RuntimeError; end
    class UnsupportedOSversion < RuntimeError; end
    class NoWorkerName < RuntimeError; end
    class NoComputeUnits < RuntimeError; end
    class InvalidPCIDatabase < RuntimeError; end
  end
end
