# frozen_string_literal: true

require 'compute_unit/exceptions'
require 'etc'
module ComputeUnit
  module Utils
    # @return [Boolean] - return true if the current user is root
    def self.root?
      ::Etc.getpwuid.name == 'root'
    end

    # @return [Boolean] - return true if the current user is root
    def root?
      ::Etc.getpwuid.name == 'root'
    end

    # @return [Boolean] - returns true if user is root
    # @raises [Crossbelt::Exceptions::NoPermission] if user does not have permission
    def check_for_root
      raise Exceptions::NoPermission.new('Please run this command as root or with sudo') unless root?

      root?
    end
    module_function :check_for_root
  end
end
