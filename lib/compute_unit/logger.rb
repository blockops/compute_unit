# frozen_string_literal: true

require 'logger'

module ComputeUnit
  module Logger
    def self.log_file
      if ENV['LOG_FILENAME'] && File.exist?(ENV['LOG_FILENAME'])
        ENV['LOG_FILENAME']
      else
        STDERR
      end
    end

    def self.logger
      @logger ||= begin
        log = ::Logger.new(Logger.log_file)
        log.level = log_level
        log.progname = 'ComputeUnit'
        log.formatter = proc do |severity, datetime, progname, msg|
          if Logger.log_file == STDERR
            "#{severity} - #{progname}: #{msg}\n".send(color(severity))
          else
            "#{datetime} #{severity} - #{progname}: #{msg}\n".send(color(severity))
          end
        end
        log
      end
    end

    def logger
      @logger ||= Logger.logger
    end

    def self.color(severity)
      case severity
      when ::Logger::Severity::WARN, 'WARN'
        :yellow
      when ::Logger::Severity::INFO, 'INFO'
        :green
      when ::Logger::Severity::FATAL, 'FATAL'
        :fatal
      when ::Logger::Severity::ERROR, 'ERROR'
        :fatal
      when ::Logger::Severity::DEBUG, 'DEBUG'
        :green
      else
        :green
      end
    end

    def self.log_level
      level = ENV['LOG_LEVEL'].downcase if ENV['LOG_LEVEL']
      case level
      when 'warn'
        ::Logger::Severity::WARN
      when 'fatal'
        ::Logger::Severity::FATAL
      when 'debug'
        ::Logger::Severity::DEBUG
      when 'info'
        ::Logger::Severity::INFO
      when 'error'
        ::Logger::Severity::ERROR
      else
        ::Logger::Severity::INFO
      end
    end
  end
end
