# frozen_string_literal: true

class File
  # @param path [String] - the path to the file
  # @param n [Integer] - the number of lines to read from the path
  # @summary Reads N lines from the end of file, without reading the entire file into memory
  # @return [String] -  the data read from the file
  def self.tail(path, n = 1)
    return '' unless File.exist?(path)

    File.open(path, 'r') do |file|
      buffer_s = 512
      line_count = 0
      file.seek(0, IO::SEEK_END)

      offset = file.pos # we start at the end

      while line_count <= n && offset > 0
        to_read = if (offset - buffer_s) < 0
                    offset
                  else
                    buffer_s
                  end

        file.seek(offset - to_read)
        data = file.read(to_read)

        data.reverse.each_char do |c|
          if line_count > n
            offset += 1
            break
          end
          offset -= 1
          line_count += 1 if c == "\n"
        end
      end
      file.seek(offset)
      file.read
    end
  end
end

class String
  def colorize(color_code)
    "\e[#{color_code}m#{self}\e[0m"
  end

  def red
    colorize(31)
  end

  def green
    colorize(32)
  end

  def fatal
    red
  end

  def yellow
    colorize(33)
  end
end

class Hash
  def stringify_keys
    each_with_object({}) do |(key, value), hash|
      value = value.stringify_keys if value.is_a?(Hash)
      hash[key.to_s] = value
    end
  end

  def symbolize_keys(&select)
    dup.symbolize_keys!(&select)
  end

  def symbolize_keys!(&select)
    if select
      keys.each do |key|
        next unless select[key]

        new_key = (begin
          key.to_sym
                   rescue StandardError
                     key.to_s.to_sym
        end)
        self[new_key] = delete(key)
      end
    else
      keys.each do |key|
        new_key = (begin
          key.to_sym
                   rescue StandardError
                     key.to_s.to_sym
        end)
        self[new_key] = delete(key)
      end
    end
    self
  end
end
