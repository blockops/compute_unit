# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ComputeUnit::Device do
  include_context 'common_mocks'
  let(:opts) do
    {
      device_class_id: '030000',
      device_id: '1b81',
      device_vendor_id: '10de',
      subsystem_vendor_id: '1458',
      subsystem_device_id: '3701'
    }
  end

  let(:device_path) { '/sys/bus/pci/devices/0000:06:00.0' }

  let(:device) do
    ComputeUnit::Device.new(device_path, opts)
  end

  describe 'multiple devices' do
    let(:opts) do
      {
        device_class_id: '030000',
        device_id: '1b81',
        device_vendor_id: '10de',
        subsystem_vendor_id: '1458',
        subsystem_device_id: '3701'
      }
    end

    let(:devices) do
      [
        '/sys/bus/pci/devices/0000:06:00.0',
        '/sys/bus/pci/devices/0000:03:00.0',
        '/sys/bus/pci/devices/0000:02:00.0',
        '/sys/bus/pci/devices/0000:01:00.0'
      ]
    end

    before(:each) do
      devices.each do |device_path|
        allow(ComputeUnit::Device).to receive(:create_from_path).with(device_path).and_return(ComputeUnit::Device.new(device_path, opts))
      end
      allow(Dir).to receive(:glob).with('/sys/bus/pci/devices/*').and_return(devices)
    end

    it '#find_all be array' do
      expect(ComputeUnit::Device.find_all).to be_a Array
    end

    it '#find_all return devices' do
      expect(ComputeUnit::Device.find_all.first).to be_a ComputeUnit::Device
    end

    it '#find_all return all devices' do
      expect(ComputeUnit::Device.find_all.count).to eq(4)
    end
  end

  describe '#single device' do
    before(:each) do
      allow(device).to receive(:base_hwmon_path).and_return(File.join(fixtures_dir, 'device_path', 'hwmon'))
    end

    it '#new' do
      expect(device).to be_a ComputeUnit::Device
    end

    it '#new is not slow' do
      data = Benchmark.measure do |_x|
        ComputeUnit::Device.new('/sys/bus/pci/devices/0000:01:00.0', opts)
      end
      expect(data.total.round(4)).to be <= 0.01
    end

    it 'to_h is not slow' do
      data = Benchmark.measure do |_x|
        device.to_h
      end
      expect(data.total.round(4)).to be <= 0.09
    end

    it 'loading database is not slow' do
      data = Benchmark.measure do |_x|
        ComputeUnit::Device.pci_database
      end
      expect(data.total.round(4)).to be <= 0.01
    end

    it '#model' do
      expect(device.model).to eq('GeForce GTX 1070')
    end

    it 'make is not slow' do
      data = Benchmark.measure do |_x|
        device.make
      end
      expect(data.total.round(4)).to be <= 0.02
    end

    it 'model is not slow' do
      data = Benchmark.measure do |_x|
        device.model
      end
      expect(data.total.round(4)).to be <= 0.06
    end

    it 'vendor is not slow' do
      data = Benchmark.measure do |_x|
        device.vendor
      end
      expect(data.total.round(4)).to be <= 0.05
    end

    it '#make' do
      expect(device.make).to eq('Nvidia')
    end

    it '#vendor' do
      expect(device.vendor).to eq('Gigabyte')
    end

    it '#to_h' do
      expect(device.to_h).to be_a Hash
    end

    it 'show list keys' do
      expect(device.to_h.keys).to eq(%i[chip_make make model device_id vendor_id subsystem_device_id subsystem_vendor_id device_class])
    end

    it 'show list values' do
      expect(device.to_h.values).to eq(['Nvidia', 'Gigabyte', 'GeForce GTX 1070', '1b81', '10de', '3701', '1458', '030000'])
    end

    it '#hwmon_path' do
      expect(device.hwmon_path).to eq(File.join(fixtures_dir, 'device_path', 'hwmon', 'hwmon3'))
    end

    it '#write_hwmon' do
      expect(device.write_hwmon_data('bogus', 'value'))
    end
  end
end
