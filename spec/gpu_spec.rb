# frozen_string_literal: true

require 'spec_helper'
require 'compute_unit/gpu'
require 'compute_unit/gpus/amd_gpu'
require 'compute_unit/gpus/nvidia_gpu'

RSpec.describe ComputeUnit::Gpu do
  include_context 'common_mocks'
  let(:opts) do
    {
      device_class_id: '030000',
      device_id: '1b81',
      device_vendor_id: '10de',
      subsystem_vendor_id: '1458',
      subsystem_device_id: '3701',
      model: 'Radeon RX 580'
    }
  end

  let(:device_paths) do
    [
      '/sys/bus/pci/devices/0000:06:00.0',
      '/sys/bus/pci/devices/0000:03:00.0',
      '/sys/bus/pci/devices/0000:02:00.0',
      '/sys/bus/pci/devices/0000:01:00.0'
    ]
  end

  let(:devices) do
    device_paths.map do |path|
      ComputeUnit.Device.new(path, {})
    end
  end

  before(:each) do
    allow_any_instance_of(ComputeUnit::Device).to receive(:device_class).and_return('030000')
    allow_any_instance_of(ComputeUnit::Device).to receive(:device_vendor).and_return('1002')

    allow(ComputeUnit::Gpu).to receive(:model).and_return('RX 580')

    allow(ComputeUnit::Gpu).to receive(:devices).and_return(device_paths)
  end

  let(:gpu) do
    ComputeUnit::Gpu.new('device_path', {})
  end

  it '#new' do
    expect(gpu).to be_a ComputeUnit::Gpu
  end

  it '#device_class_name' do
    expect(gpu.device_class_name).to eq('GPU')
  end

  it '#find_all' do
    expect(ComputeUnit::Gpu.find_all).to be_a Array
  end

  it 'devices is array' do
    expect(ComputeUnit::Gpu.devices).to be_a Array
    expect(ComputeUnit::Gpu.devices.count).to eq(4)
  end

  describe 'all' do
    include_context :amd_computes

    let(:information) do
      { model: 'GeForce GTX 1070',
        irq: '130',
        gpu_uuid: 'GPU-0116fb5c-66f4-1cba-c216-97f4600a8152',
        video_bios: '86.04.50.40.4A',
        bus_type: 'PCIe',
        dma_size: '47 bits',
        dma_mask: '0x7fffffffffff',
        bus_location: '0000:0d:00.0',
        device_minor: '7' }
    end
  end
end
