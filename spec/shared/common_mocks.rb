# frozen_string_literal: true

require 'spec_helper'
require 'compute_unit/gpus/nvidia_gpu'
require 'compute_unit/gpus/amd_gpu'

RSpec.shared_context 'common_mocks' do
  before(:each) do
    allow_any_instance_of(ComputeUnit::ComputeBase).to receive(:device_lookup).and_return('Asus 1070')
    allow_any_instance_of(ComputeUnit::CacheStore).to receive(:write_json_cache_file).and_return('blah')
    allow(::ComputeUnit::ComputeBase).to receive(:pci_database).and_return(pci_database)
    stub_const('ComputeUnit::PCI_DATABASE_PATH', File.join(fixtures_dir, 'pci.ids'))
    allow_any_instance_of(ComputeUnit::AmdGpu).to receive(:unlock_rom).and_return(true)
    allow_any_instance_of(ComputeUnit::AmdGpu).to receive(:lock_rom).and_return(true)
  end
end

RSpec.shared_context :amd_computes do
  let(:opts) do
    {
      device_class_id: '030000',
      device_id: '67df',
      device_vendor_id: '1002',
      subsystem_vendor_id: '1682',
      subsystem_device_id: '9580',
      bios: '113-P20-XTX-I1366M8G-W82',
      model: 'Radeon RX 580'
    }
  end

  before(:each) do
    allow(ComputeUnit::Gpu).to receive(:devices).and_return(devices)
    allow_any_instance_of(ComputeUnit::AmdGpu).to receive(:device_class_name).and_return('GPU')
    allow_any_instance_of(ComputeUnit::AmdGpu).to receive(:read_kernel_setting).with('pp_power_usage', 0).and_return('100')
    allow_any_instance_of(ComputeUnit::AmdGpu).to receive(:temperature).and_return(54)
    allow_any_instance_of(ComputeUnit::AmdGpu).to receive(:fan_limit).and_return(54)
    allow_any_instance_of(ComputeUnit::AmdGpu).to receive(:fan).and_return(2300)
    allow_any_instance_of(ComputeUnit::AmdGpu).to receive(:temp).and_return(53)
    allow_any_instance_of(ComputeUnit::AmdGpu).to receive(:memory_clock).and_return(1950)
    allow_any_instance_of(ComputeUnit::AmdGpu).to receive(:core_clock).and_return(1100)
    allow_any_instance_of(ComputeUnit::AmdGpu).to receive(:core_voltage).and_return(900)
    allow_any_instance_of(ComputeUnit::AmdGpu).to receive(:fan_min_limit).and_return(0)
    allow_any_instance_of(ComputeUnit::AmdGpu).to receive(:fan_max_limit).and_return(100)
    allow_any_instance_of(ComputeUnit::AmdGpu).to receive(:read_dri_debug_file).and_return(File.join(fixtures_dir, 'amdgpu_pm_info.txt'))
    allow_any_instance_of(ComputeUnit::AmdGpu).to receive(:read_kernel_setting)
      .with('pp_od_clk_limits', '0 0')
      .and_return("Sclk Limit: 2000 Mhz\nMclk Limit: 2250 Mhz\n")
  end

  let(:devices) do
    ['/sys/bus/pci/devices/0000:0d:00.0',
     '/sys/bus/pci/devices/0000:0d:00.1',
     '/sys/bus/pci/devices/0000:0d:00.2',
     '/sys/bus/pci/devices/0000:0d:00.3',
     '/sys/bus/pci/devices/0000:0d:00.4',
     '/sys/bus/pci/devices/0000:0d:00.5']
  end

  let(:computes) do
    devices.map.with_index do |path, index|
      ComputeUnit::AmdGpu.new(path, opts.merge(index: index))
    end
  end
end

RSpec.shared_context :nvidia_computes do
  let(:information) do
    { model: 'GeForce GTX 1070',
      irq: '130',
      gpu_uuid: 'GPU-0116fb5c-66f4-1cba-c216-97f4600a8152',
      video_bios: '86.04.50.40.4a',
      bus_type: 'PCIe',
      dma_size: '47 bits',
      dma_mask: '0x7fffffffffff',
      bus_location: '0000:0d:00.0',
      device_minor: '7' }
  end

  let(:opts) do
    {
      device_class_id: '030000',
      device_id: '1b81',
      device_vendor_id: '10de',
      subsystem_vendor_id: '1458',
      subsystem_device_id: '3701'
    }
  end

  let(:meta) do
    {
      'utilization.gpu [%]' => '100',
      'power.draw [W]' => '100',
      'memory.total [MiB]' => '8117',
      'memory.free [MiB]' => '5196',
      'memory.used [MiB]' => '2921',
      'temperature.gpu' => '54',
      'power.limit [W]' => '120.0',
      'power.max_limit [W]' => '180.0',
      'fan.speed [%]' => '55',
      'pstate' => 'P2',
      'clocks.current.memory [MHz]' => '4404',
      'clocks.current.sm [MHz]' => '1480'
      # name,GeForce GTX 1070
      # vbios_version,86.04.50.00.64
      # uuid,GPU-850cb068-da96-c113-a76b-d629e93bec35

    }
  end

  before(:each) do
    allow(ComputeUnit::NvidiaGpu).to receive(:read_information_file).with(anything).and_return(information)
    computes.each.with_index do |_cu, index|
      allow_any_instance_of(ComputeUnit::NvidiaGpu).to receive(:`).with("/usr/bin/nvidia-smi --query-gpu=gpu_name,vbios_version,uuid,memory.used,memory.free,memory.total,utilization.gpu,temperature.gpu,power.draw,power.limit,power.max_limit,fan.speed,pstate,clocks.current.memory,clocks.current.sm -i #{index} --format=csv,nounits 2>&1").and_return(File.read(File.join(nvidia_fixtures, 'raw_data_single.txt')))
    end
    allow_any_instance_of(ComputeUnit::NvidiaGpu).to receive(:device_class_name).and_return('GPU')
    allow_any_instance_of(ComputeUnit::NvidiaGpu).to receive(:meta).and_return(meta)
    allow_any_instance_of(ComputeUnit::NvidiaGpu).to receive(:temperature).and_return(54)
    # allow_any_instance_of(ComputeUnit::NvidiaGpu).to receive(:power).and_return(54)
    allow_any_instance_of(ComputeUnit::NvidiaGpu).to receive(:read_information_file).and_return('')
    allow(ComputeUnit::Gpu).to receive(:devices).and_return(devices)
  end

  let(:devices) do
    ['/sys/bus/pci/devices/0000:0d:00.0',
     '/sys/bus/pci/devices/0000:0d:00.1',
     '/sys/bus/pci/devices/0000:0d:00.2',
     '/sys/bus/pci/devices/0000:0d:00.3',
     '/sys/bus/pci/devices/0000:0d:00.4',
     '/sys/bus/pci/devices/0000:0d:00.5']
  end

  let(:computes) do
    devices.map.with_index do |path, index|
      ComputeUnit::NvidiaGpu.new(path, opts.merge(index: index))
    end
  end
end
