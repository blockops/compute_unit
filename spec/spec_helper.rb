# frozen_string_literal: true

require 'bundler/setup'
require 'compute_unit'
require 'pry'
require 'shared/common_mocks'
require 'benchmark'
require 'simplecov'

SimpleCov.start do
  add_filter '.bundle'
end

def sysfs_dir
  File.join(File.dirname(__FILE__), 'fixtures', 'sysfs', 'sys')
end

def fixtures_dir
  File.join(File.dirname(__FILE__), 'fixtures')
end

def pci_database
  @pci_database ||= File.readlines(File.join(fixtures_dir, 'pci.ids'))
end

def nvidia_fixtures
  File.join(fixtures_dir, 'nvidia')
end

def amd_fixtures
  File.join(fixtures_dir, 'amd')
end

def lib_dir
  File.dirname(File.dirname(__FILE__))
end

def with_captured_stdout
  old_stdout = $stdout
  $stdout = StringIO.new
  yield if block_given?
  $stdout.string
ensure
  $stdout = old_stdout
end

# export RUBYLIB=$RUBYLIB:~/nwops/crossbelt/lib
RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end
