# frozen_string_literal: true

# frozen_string_literal: true

require 'spec_helper'
require 'compute_unit'
require 'compute_unit/gpus/amd_gpu'
require 'compute_unit/gpus/nvidia_gpu'

RSpec.describe ComputeUnit do
  include_context 'common_mocks'

  before(:each) do
    allow_any_instance_of(ComputeUnit::AmdGpu).to receive(:opencl_units).and_return(32)
    allow_any_instance_of(ComputeUnit::AmdGpu).to receive(:opencl_board_name).and_return('Radeon (TM) RX 480 Graphics')
  end

  let(:cu) do
    subject
  end

  describe 'cpu' do
    it '#find_all' do
      expect(ComputeUnit.find_all).to be_a Array
    end
  end

  describe 'asic' do
    it '#find_all' do
      expect(ComputeUnit.find_all).to be_a Array
    end
  end

  describe 'gpu' do
    describe 'nvidia' do
      let(:information) do
        { model: 'GeForce GTX 1070',
          irq: '130',
          gpu_uuid: 'GPU-0116fb5c-66f4-1cba-c216-97f4600a8152',
          video_bios: '86.04.50.40.4a',
          bus_type: 'PCIe',
          dma_size: '47 bits',
          dma_mask: '0x7fffffffffff',
          bus_location: '0000:0d:00.0',
          device_minor: '7' }
      end

      before(:each) do
        allow(ComputeUnit::NvidiaGpu).to receive(:read_information_file).and_return(information)
        allow(ComputeUnit::Gpu).to receive(:find_all).and_return(computes)
      end

      let(:computes) do
        [
          ComputeUnit::NvidiaGpu.new('/sys/bus/pci/devices/0000:06:00.0', index: 0),
          ComputeUnit::NvidiaGpu.new('/sys/bus/pci/devices/0000:03:00.0', index: 0),
          ComputeUnit::NvidiaGpu.new('/sys/bus/pci/devices/0000:02:00.0', index: 0),
          ComputeUnit::NvidiaGpu.new('/sys/bus/pci/devices/0000:01:00.0', index: 0)
        ]
      end

      it '#find_all' do
        expect(ComputeUnit.find_all).to be_a Array
      end
    end

    describe 'amd' do
      before(:each) do
        allow(ComputeUnit::Gpu).to receive(:find_all).and_return(computes)
      end

      let(:computes) do
        [
          ComputeUnit::AmdGpu.new('/sys/bus/pci/devices/0000:06:00.0', index: 0),
          ComputeUnit::AmdGpu.new('/sys/bus/pci/devices/0000:03:00.0', index: 0),
          ComputeUnit::AmdGpu.new('/sys/bus/pci/devices/0000:02:00.0', index: 0),
          ComputeUnit::AmdGpu.new('/sys/bus/pci/devices/0000:01:00.0', index: 0)
        ]
      end

      it '#find_all' do
        expect(ComputeUnit.find_all).to be_a Array
      end
    end
  end
end
