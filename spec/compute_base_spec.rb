# frozen_string_literal: true

require 'spec_helper'
require 'compute_unit/cpu'

RSpec.describe ComputeUnit::ComputeBase do
  before(:each) do
    allow(ComputeUnit::ComputeBase).to receive(:pci_database).and_return(File.readlines(File.join(fixtures_dir, 'pci.ids')))
  end

  let(:klass) do
    ComputeUnit::ComputeBase
  end

  let(:cpu) do
    ComputeUnit::ComputeBase.new('/sys/bus/pci/devices/0000:0f:00.0', index: 0)
  end

  it '#new' do
    expect(cpu).to be_a ComputeUnit::ComputeBase
  end

  it '#power_offset=20' do
    allow(cpu).to receive(:power).and_return(100)
    cpu.power_offset = 20
    expect(cpu.power_offset).to eq(20)
  end

  it '#power_offset=-20' do
    allow(cpu).to receive(:power).and_return(100)
    cpu.power_offset = -20
    expect(cpu.power_offset).to eq(-20)
  end

  it '#power_offset=-14%' do
    allow(cpu).to receive(:power).and_return(100)
    cpu.power_offset = -0.1452
    expect(cpu.power_offset).to eq(-15)
  end

  it '#power_offset=30%' do
    allow(cpu).to receive(:power).and_return(100)
    cpu.power_offset = 0.3
    expect(cpu.power_offset).to eq(30)
  end

  it '#power_offset=nil' do
    allow(cpu).to receive(:power).and_return(100)
    cpu.power_offset = nil
    expect(cpu.power_offset).to eq(0)
  end

  it '#find_all' do
    expect(ComputeUnit::ComputeBase.find_all).to be_a Array
  end

  it '#vendor_lookup' do
    expect(klass.vendor_lookup('1002')).to eq('AMD')
  end

  it '#device_lookup' do
    expect(klass.device_lookup('aaf0')).to eq('Radeon RX 580')
  end

  it '#subsystem_device_lookup' do
    expect(klass.subsystem_device_lookup('aaf0', '04fb', '1043')).to eq('Radeon RX 480')
  end

  it '#subsystem_vendor_lookup' do
    expect(klass.vendor_lookup('1682')).to eq('XFX')
  end

  describe 'vendor and subsystem vendor' do
    it '#vendor_lookup' do
      expect(klass.vendor_lookup('1002')).to eq('AMD')
    end

    it '#device_lookup' do
      expect(klass.device_lookup('687f')).to eq('Radeon RX Vega 64')
    end

    it '#subsystem_device_lookup' do
      expect(klass.subsystem_device_lookup('687f', '0b36', '1002')).to eq('Radeon RX Vega 64')
    end

    it '#subsystem_vendor_lookup' do
      expect(klass.vendor_lookup('1002')).to eq('AMD')
    end
  end

  describe 'model with brackets' do
    it '#device_lookup' do
      expect(klass.device_lookup('aaf0')).to eq('Radeon RX 580')
    end

    it '#subsystem_device_lookup default to device' do
      expect(klass.subsystem_device_lookup('aaf0', 'aaf0', '1682')).to be_nil
    end

    it '#subsystem_vendor_lookup' do
      expect(klass.vendor_lookup('1682')).to eq('XFX')
    end
  end
end
