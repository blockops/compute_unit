# frozen_string_literal: true

require 'crossbelt/compute_unit/gpus/amd_utils'
require 'crossbelt/compute_unit/gpus/nvidia_gpu'
require 'spec_helper'

def processed_data
  [{ gpu_id: 'GPU0',
     pci_loc: '0c.00.0',
     card_name: 'Radeon RX 570',
     bios_name: '115-C940PI0-100',
     memory_name: 'Elpida EDW4032BABG',
     memory_type: 'GDDR5',
     gpu_platform: 'Polaris10',
     pci_checksum: nil,
     temp: '41',
     load: '100',
     power: 142,
     fanrpm: 1000 },
   { gpu_id: 'GPU1',
     pci_loc: '10.00.1',
     card_name: 'Radeon RX 570',
     bios_name: '115-C940PI0-100',
     memory_name: 'Elpida EDW4032BABG',
     memory_type: 'GDDR5',
     gpu_platform: 'Polaris10',
     pci_checksum: nil,
     temp: '41',
     load: '100',
     power: 143,
     fanrpm: 1000 },
   { gpu_id: 'GPU2',
     pci_loc: '10.00.2',
     card_name: 'Radeon RX 570',
     bios_name: '115-C940PI0-100',
     memory_name: 'Elpida EDW4032BABG',
     memory_type: 'GDDR5',
     gpu_platform: 'Polaris10',
     pci_checksum: nil,
     temp: '41',
     load: '100',
     power: 144,
     fanrpm: 1000 }]
end
