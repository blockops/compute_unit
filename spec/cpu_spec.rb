# frozen_string_literal: true

require 'spec_helper'
require 'compute_unit/cpu'

RSpec.describe ComputeUnit::Cpu do
  let(:cpu) do
    ComputeUnit::Cpu.new('/sys/bus/pci/devices/0000:0f:00.0', index: 0)
  end

  before(:each) do
    allow(cpu).to receive(:base_hwmon_path).and_return(File.join(sysfs_dir, 'devices', 'platform', 'coretemp.0', 'hwmon'))
    allow(cpu).to receive(:`).with('lscpu').and_return(File.read(File.join(fixtures_dir, 'lscpu.txt')))
  end

  it do
    expect(cpu).to be_a ComputeUnit::Cpu
  end

  it 'temp' do
    expect(cpu.temp).to eq(27)
  end

  it '#temps' do
    expect(cpu.temps).to eq(core_0: 31, core_1: 31, package_id_0: 27)
  end

  it '#find_all' do
    expect(ComputeUnit::Cpu.find_all).to be_a Array
  end

  it '#device_class_name' do
    expect(cpu.device_class_name).to eq('CPU')
  end

  it '#make' do
    expect(cpu.make).to eq('GenuineIntel')
  end

  it '#model' do
    expect(cpu.model).to eq('Intel(R) Core(TM) i3-7100 CPU @ 3.90GHz')
  end

  it '#num_cores' do
    expect(cpu.num_cores).to eq(2)
  end

  it '#num_threads' do
    expect(cpu.num_threads).to eq(2)
  end

  it '#nproc' do
    expect(cpu.nproc).to eq(4)
  end

  it '#min_freq_mhz' do
    expect(cpu.min_freq_mhz).to eq(800)
  end

  it '#max_freq_mhz' do
    expect(cpu.max_freq_mhz).to eq(3900)
  end

  it '#current_freq_mhz' do
    expect(cpu.current_freq_mhz).to eq(801)
  end

  it '#to_h' do
    expect(cpu.to_h).to eq(
      chip_make: 'GenuineIntel',
      make: 'GenuineIntel',
      model: 'Intel(R) Core(TM) i3-7100 CPU @ 3.90GHz',
      device_id: nil,
      vendor_id: nil,
      uuid: 'CPU0',
      subsystem_device_id: nil,
      subsystem_vendor_id: nil,
      device_class: nil,
      temp: 27,
      minFreqMhz: 800,
      maxFreqMhz: 3900,
      currentFreqMhz: 801,
      numCores: 2,
      numThreads: 2,
      nproc: 4,
      temps: { core_1: 31, package_id_0: 27, core_0: 31 }
    )
  end
end
