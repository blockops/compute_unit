# frozen_string_literal: true

require 'spec_helper'
require 'compute_unit/gpus/nvidia_gpu'

RSpec.describe ComputeUnit::NvidiaGpu do
  include_context 'common_mocks'

  let(:meta) do
    {
      'name' => 'GeForce GTX 1070 Ti',
      'vbios_version' => '86.04.85.00.63',
      'uuid' => 'GPU-a583cb04-f9b5-68f3-50b9-2b4ba1c7d14e',
      'memory.used [MiB]' => '2578',
      'memory.free [MiB]' => '5534',
      'memory.total [MiB]' => '8112',
      'utilization.gpu [%]' => '100 %',
      'temperature.gpu' => '53',
      'power.draw [W]' => '129.21',
      'power.limit [W]' => '130.00',
      'power.max_limit [W]' => '217.00',
      'pstate' => 2,
      'fan.speed [%]' => '75',
      'clocks.current.memory [MHz]' => '4400',
      'clocks.current.sm [MHz]' => '1500'
    }
  end

  let(:opts) do
    {
      device_class_id: '030000',
      device_id: '1b81',
      device_vendor_id: '10de',
      subsystem_vendor_id: '1458',
      subsystem_device_id: '3701',
      index: 0
    }
  end

  let(:information) do
    { model: 'GeForce GTX 1070',
      irq: '130',
      gpu_uuid: 'GPU-0116fb5c-66f4-1cba-c216-97f4600a8152',
      video_bios: '86.04.50.40.4A',
      bus_type: 'PCIe',
      dma_size: '47 bits',
      dma_mask: '0x7fffffffffff',
      bus_location: '0000:0d:00.0',
      device_minor: '7' }
  end

  let(:gpu) do
    ComputeUnit::NvidiaGpu.new('/sys/bus/pci/devices/0000:0f:00.0', opts)
  end

  before(:each) do
    allow(ComputeUnit::NvidiaGpu).to receive(:read_information_file).and_return(information)
    allow(gpu).to receive(:subsystem_vendor).and_return('1458')
    allow(gpu).to receive(:device_vendor).and_return('10de')
    allow(gpu).to receive(:subsystem_device).and_return('3701')
    allow(gpu).to receive(:metadata).and_return(meta)
    allow(gpu).to receive(:device).and_return('1b81')
  end

  it '#new' do
    expect(gpu).to be_a ComputeUnit::NvidiaGpu
  end

  it '#find_all' do
    expect(ComputeUnit::NvidiaGpu.find_all).to be_a Array
  end

  it '#gpus' do
    expect(ComputeUnit::NvidiaGpu.devices).to be_a Array
  end

  it '#power' do
    # allow(gpu).to receive(:)
    expect(gpu.power).to eq 129.21
  end

  it '#fan' do
    expect(gpu.fan).to eq 75
  end

  it '#temp' do
    expect(gpu.temp).to eq 53
  end

  it '#meta' do
    expect(gpu.meta).to be_a Hash
  end

  it '#pstate' do
    expect(gpu.pstate).to eq 2
  end

  it '#to_json' do
    json_data = gpu.to_json
    expect(json_data).to be_a String
    expect(JSON.parse(json_data)).to be_a Hash
  end

  it '#to_h' do
    expect(gpu.to_h).to be_a Hash
  end

  it '#status of 1' do
    expect(gpu.status).to eq(0)
  end

  it '#status of 0' do
    expect(gpu.status).to eq(0)
  end

  it '#proper hash keys' do
    expect(gpu.to_h.keys).to eq(%i[uuid gpuId syspath pciLoc name bios subType make model vendor
                                   power utilization temperature status pstate fanSpeed
                                   type
                                   maxTemp
                                   mem
                                   cor
                                   vlt
                                   mem_temp
                                   maxFan
                                   dpm
                                   vddci
                                   maxPower
                                   ocProfile
                                   opencl_enabled])
  end

  it 'contains a bios' do
    expect(gpu.bios).to eq('86.04.50.40.4A')
  end

  it '#core_voltage' do
    expect(gpu.core_voltage).to eq(0)
  end

  it '#configured_core_voltage' do
    expect(gpu.configured_core_voltage).to eq(0)
  end

  it '#memory_clock' do
    expect(gpu.memory_clock).to eq(4400)
  end

  it '#core_clock' do
    expect(gpu.core_clock).to eq(1500)
  end

  it 'contains a make' do
    expect(gpu.make).to eq('Nvidia')
  end

  it 'contains a model' do
    expect(gpu.model).to eq('GeForce GTX 1070')
  end

  it 'contains a vendor' do
    expect(gpu.vendor).to eq('Gigabyte')
  end

  it 'contains a device_path' do
    expect(gpu.device_path).to eq('/sys/bus/pci/devices/0000:0f:00.0')
  end

  it 'contains a serial' do
    # currently we cannot track serial numbers
    expect(gpu.serial).to be_nil
  end

  it 'contains a pci_loc' do
    expect(gpu.pci_loc).to eq('0000:0d:00.0')
  end

  it '#status of 1' do
    allow(gpu).to receive(:utilization).and_return(0)
    expect(gpu.status).to eq(1)
  end

  it '#status of 0' do
    allow(gpu).to receive(:utilization).and_return(100)
    expect(gpu.status).to eq(0)
  end

  it '#status of 1 with no power' do
    allow(gpu).to receive(:utilization).and_return(100)
    allow(gpu).to receive(:power).and_return(0)
    expect(gpu.status).to eq(2)
  end

  %i[
    meta
    pstate
    power_limit
    power_max_limit
    utilization
    memory_used
    memory_free
    memory_total
    to_h
    index
    status
    compute_type
    type
    uuid
  ].each do |m|
    it m do
      expect(gpu.send(m)).to_not be_nil
    end
  end

  describe 'all' do
    include_context :nvidia_computes

    it '#self.find_all return only nvidia' do
      allow(ComputeUnit::NvidiaGpu).to receive(:device_vendor).and_return('10de')
      # allow(ComputeUnit::Gpu).to receive(:found_devices).and_return(computes.map(&:device_path))
      allow(ComputeUnit::NvidiaGpu).to receive(:device_vendor).with('/sys/bus/pci/devices/0000:01:00.0').and_return('1009')
      units = ComputeUnit::NvidiaGpu.find_all.map(&:device_path)
      expect(units).to eq(['/sys/bus/pci/devices/0000:0d:00.0', '/sys/bus/pci/devices/0000:0d:00.1',
                           '/sys/bus/pci/devices/0000:0d:00.2', '/sys/bus/pci/devices/0000:0d:00.3',
                           '/sys/bus/pci/devices/0000:0d:00.4', '/sys/bus/pci/devices/0000:0d:00.5'])
    end
  end
end
