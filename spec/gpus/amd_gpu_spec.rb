# frozen_string_literal: true

require 'spec_helper'
require 'compute_unit/gpus/amd_gpu'
require 'compute_unit/compute_base'

RSpec.describe ComputeUnit::AmdGpu do
  include_context 'common_mocks'

  let(:opts) do
    {
      device_class_id: '030000',
      device_id: 'aaf0',
      device_vendor_id: '1002',
      subsystem_vendor_id: '1682',
      subsystem_device_id: 'aaf0',
      index: 0
    }
  end

  let(:gpu) do
    ComputeUnit::AmdGpu.new('/sys/bus/pci/devices/0000:0f:00.0', opts)
  end

  let(:worker_data) do
    [{ busid: '03.00.0', name: 'Radeon RX 570', accepted: 650, rejected: 0, temperature: 65, power_usage: 138, speed: 26_185_000, efficency: 26185.0, invalid: 49 },
     { busid: '08.00.0', name: 'Radeon RX 570', accepted: 650, rejected: 0, temperature: 53, power_usage: 139, speed: 26_725_000, efficency: 168.0, invalid: 49 },
     { busid: '0a.00.0', name: 'Radeon RX 570', accepted: 650, rejected: 0, temperature: 65, power_usage: 140, speed: 27_410_000, efficency: 27410.0, invalid: 49 },
     { busid: '0b.00.0', name: 'Radeon RX 570', accepted: 650, rejected: 0, temperature: 68, power_usage: 141, speed: 27_408_000, efficency: 27408.0, invalid: 49 },
     { busid: '0d.00.0', name: 'Radeon RX 570', accepted: 650, rejected: 0, temperature: 61, power_usage: 142, speed: 27_405_000, efficency: 27405.0, invalid: 49 },
     { busid: '0e.00.0', name: 'Radeon RX 570', accepted: 650, rejected: 0, temperature: 59, power_usage: 143, speed: 27_396_000, efficency: 27396.0, invalid: 49 },
     { busid: '10.00.0', name: 'Radeon RX 570', accepted: 650, rejected: 0, temperature: 70, power_usage: 144, speed: 27_442_000, efficency: 27442.0, invalid: 49 }]
  end

  before(:each) do
    stub_const('ComputeUnit::SYSFS_PATH', sysfs_dir)

    allow_any_instance_of(ComputeUnit::AmdGpu).to receive(:opencl_units).and_return(32)

    allow_any_instance_of(ComputeUnit::AmdGpu).to receive(:unlock_rom).and_return('1')
    allow_any_instance_of(ComputeUnit::AmdGpu).to receive(:lock_rom).and_return('0')
    allow_any_instance_of(ComputeUnit::AmdGpu).to receive(:debug_rom_path).and_return(File.join(fixtures_dir, 'roms', 'amdgpu-rom.bin'))
    allow_any_instance_of(ComputeUnit::AmdGpu).to receive(:rom_path).and_return(File.join(fixtures_dir, 'roms', 'amdgpu-rom.bin'))
    allow_any_instance_of(ComputeUnit::AmdGpu).to receive(:opencl_board_name).and_return('Radeon (TM) RX 480 Graphics')
    allow(gpu).to receive(:read_hwmon_data).with('fan1_input', 0).and_return('1000')
    allow(gpu).to receive(:read_hwmon_data).with('temp3_input', 0).and_return('825')
    allow(gpu).to receive(:read_hwmon_data).with('temp1_input', 0).and_return('41000')
    allow(gpu).to receive(:read_kernel_setting).with('pp_power_usage', 0).and_return('142')
    allow(gpu).to receive(:read_kernel_setting).with('pp_core_vddc', 0).and_return('811')
    allow(gpu).to receive(:read_kernel_setting).with('pp_voltage', 0).and_return('826')
    allow(gpu).to receive(:read_kernel_setting).with('pp_dpm_mclk', '').and_return(File.read(File.join(amd_fixtures, 'pp_dpm_mclk')))
    allow(gpu).to receive(:read_kernel_setting).with('pp_dpm_sclk', '').and_return(File.read(File.join(amd_fixtures, 'pp_dpm_sclk')))

    allow(gpu).to receive(:read_hwmon_data).with('power1_cap', 0).and_return(158_000_000)
    allow(gpu).to receive(:read_hwmon_data).with('power1_cap_max').and_return(188_000_000)

    allow(gpu).to receive(:read_kernel_setting).with('vbios_version', 'unreadable').and_return('115-C940PI0-100')
    allow(gpu).to receive(:read_dri_debug_file).with('amdgpu_pm_info').and_return(File.read(File.join(amd_fixtures, 'amdgpu_pm_info_kernel15_1.txt')))
  end

  it '#correct debug path' do
    expect(ComputeUnit::AmdGpu::SYS_DEBUG_PATH).to eq('/sys/kernel/debug/dri')
  end

  it '#board_name' do
    expect(gpu.board_name).to eq('Radeon RX 480')
  end

  it '#new' do
    expect(gpu).to be_a ComputeUnit::AmdGpu
  end

  it '#find_all' do
    expect(ComputeUnit::AmdGpu.find_all).to be_a Array
  end

  it '#core_voltage' do
    expect(gpu.core_voltage).to eq(811)
  end

  it '#configured_core_voltage' do
    expect(gpu.configured_core_voltage).to eq(826)
  end

  it '#memory_clock' do
    expect(gpu.memory_clock).to eq(1900)
  end

  it '#core_clock' do
    expect(gpu.core_clock).to eq(1003)
  end

  it '#devices' do
    expect(ComputeUnit::AmdGpu.devices).to be_a Array
  end

  it '#power' do
    expect(gpu.power).to eq 142.0
  end

  it '#fan' do
    expect(gpu.fan).to eq 1000
  end

  it '#temp' do
    expect(gpu.temp).to eq 41
  end

  it '#meta' do
    expect(gpu.meta).to be_a Hash
  end

  it '#vendor' do
    expect(gpu.vendor).to eq('XFX')
  end

  it '#make' do
    expect(gpu.make).to eq('AMD')
  end

  it '#model' do
    expect(gpu.model).to eq('Radeon RX 480')
  end

  it '#pstate' do
    expect(gpu.pstate).to eq(-1)
  end

  it '#to_json' do
    json_data = gpu.to_json
    expect(json_data).to be_a String
    expect(JSON.parse(json_data)).to be_a Hash
  end

  it '#to_h' do
    expect(gpu.to_h).to be_a Hash
  end

  it '#status of 1' do
    allow(gpu).to receive(:power).and_return(20)
    expect(gpu.status).to eq(1)
  end

  it '#status of 0' do
    allow(gpu).to receive(:power).and_return(100)
    allow(gpu).to receive(:utilization).and_return(100)
    expect(gpu.status).to eq(0)
  end

  it '#power_limit' do
    expect(gpu.power_limit).to eq(158)
  end

  it '#power_max_limit' do
    expect(gpu.power_max_limit).to eq(188)
  end

  it '#power_limit=' do
    expect(gpu).to receive(:write_hwmon_data).with('power1_cap', 140_000_000)
    expect(gpu.power_limit = 140).to eq(140)
  end

  it '#utilization' do
    expect(gpu.utilization).to eq(100)
  end

  it '#amd_gpu_info' do
    expect(gpu.amdgpu_pm_info).to eq(mclk: { value: '1950', unit: 'MHz' },
                                     sclk: { value: '1125', unit: 'MHz' },
                                     vddgfx: { value: '950', unit: 'mV' },
                                     vddc: { value: '61.49', unit: 'W' },
                                     vddci: { value: '1.0', unit: 'W' },
                                     max_gpu: { value: '81.243', unit: 'W' },
                                     average_gpu: { value: '82.117', unit: 'W' },
                                     temperature: { value: '41', unit: 'C' },
                                     load: { value: '100', unit: '%' })
  end

  it '#power with hwmon' do
    allow(gpu).to receive(:read_kernel_setting).with('pp_power_usage', 0).and_return(0)
    allow(gpu).to receive(:read_hwmon_data).with('power1_average', 0).and_return('9000000')
    expect(gpu.power).to eq(9)
  end

  it '#amd_gpu_info with blank data' do
    allow(gpu).to receive(:read_dri_debug_file).and_return('')
    expect(gpu.amdgpu_pm_info).to eq({})
  end

  it '#status of 1' do
    allow(gpu).to receive(:utilization).and_return(0)
    expect(gpu.status).to eq(1)
  end

  it '#status of 1 with no power' do
    allow(gpu).to receive(:utilization).and_return(100)
    allow(gpu).to receive(:power).and_return(0)
    expect(gpu.status).to eq(2)
  end

  it '#voltage_table' do
    allow(gpu).to receive(:read_kernel_setting).with('pp_od_clk_voltage', nil)
                                               .and_return(File.read(File.join(amd_fixtures, 'voltage_table.txt')))
    expect(gpu.voltage_table).to eq([{ pstate: 0, clk: 300, volt: 750, type: :sclk },
                                     { pstate: 1, clk: 588, volt: 765, type: :sclk },
                                     { pstate: 2, clk: 943, volt: 776, type: :sclk },
                                     { pstate: 3, clk: 963, volt: 787, type: :sclk },
                                     { pstate: 4, clk: 983, volt: 798, type: :sclk },
                                     { pstate: 5, clk: 1003, volt: 811, type: :sclk },
                                     { pstate: 6, clk: 1023, volt: 861, type: :sclk },
                                     { pstate: 7, clk: 1043, volt: 861, type: :sclk }])
  end

  it '#proper hash keys' do
    expect(gpu.to_h.keys).to eq(%i[uuid gpuId syspath pciLoc name bios subType
                                   make model vendor power utilization temperature
                                   status pstate fanSpeed type maxTemp
                                   mem
                                   cor
                                   vlt
                                   mem_temp
                                   maxFan
                                   dpm
                                   vddci
                                   maxPower
                                   ocProfile
                                   opencl_enabled])
  end

  it '#rom_metadata' do
    expect(gpu.rom_metadata).to eq([' 761295520',
                                    '04/07/17 22:22',
                                    '113-P20-XTX-I1366M8G-W82',
                                    'PCI_EXPRESS',
                                    'C940 Polaris20 XTX A1 GDDR5 256Mx32 8GB 300e/300m                           ',
                                    '(C) 1988-2010, Advanced Micro Devices, Inc.',
                                    'ATOMBIOSBK-AMD VER015.050.002.001.000000',
                                    'I27T8L21.W82',
                                    'XFX_POLARIS20_170115JHA_XTX_SSHY_25632_C1366M8G\\config.h',
                                    'AMD ATOMBIOS'])
  end

  # unable to mock correctly
  # it '#debug_dri_dir' do
  #   # allow(gpu).to receive(:hwmon_path).with(File.join('/sys/kernel/debug/dri/', '*', 'name')).and_return(['/sys/kernel/debug/dri/0/name'])
  #   # allow(gpu).to receive(:read_file).with('/sys/kernel/debug/dri/0/name').and_return('amdgpu dev=0000:08:00.0 unique=0000:08:00.0')
  #   # require 'pry'; binding.pry
  #   expect(gpu.debug_dri_dir).to eq('')
  # end

  it '#board_name' do
    allow(ComputeUnit::Gpu).to receive(:opencl_devices_from_platform).and_return([])
    expect(gpu.board_name).to eq('Radeon RX 480')
  end

  it '#bios' do
    expect(gpu.bios).to eq('113-P20-XTX-I1366M8G-W82')
  end

  it '#status' do
    expect(gpu.status).to eq(0)
  end

  it '#uuid' do
    expect(gpu.uuid).to eq('GPU0')
  end

  it '#serial' do
    expect(gpu.serial).to eq('unknown')
  end

  it '#type' do
    expect(gpu.type).to eq(:GPU)
  end

  it '#type' do
    expect(gpu.compute_type).to eq(:GPU)
  end

  it '#pci_loc' do
    expect(gpu.pci_loc).to eq('0000:0f:00.0')
  end

  %i[meta
     fan
     pstate
     power_limit
     power_max_limit
     utilization
     memory_used
     memory_free
     memory_total
     to_h
     index
     status
     pci_loc
    ].each do |m|
    it m do
      expect(gpu.send(m)).to_not be_nil
    end
  end

  describe 'all' do
    include_context :amd_computes

    it '#self.find_all return only amd' do
      allow(ComputeUnit::AmdGpu).to receive(:device_vendor).and_return('1002')
      allow(ComputeUnit::AmdGpu).to receive(:device_vendor).with('/sys/bus/pci/devices/0000:01:00.0').and_return('1009')

      units = ComputeUnit::AmdGpu.find_all.map(&:device_path)
      expect(units).to eq(['/sys/bus/pci/devices/0000:0d:00.0', '/sys/bus/pci/devices/0000:0d:00.1',
                           '/sys/bus/pci/devices/0000:0d:00.2', '/sys/bus/pci/devices/0000:0d:00.3',
                           '/sys/bus/pci/devices/0000:0d:00.4', '/sys/bus/pci/devices/0000:0d:00.5'])
    end
  end
end
