# frozen_string_literal: true

require 'spec_helper'
require 'compute_unit/asic'

RSpec.describe ComputeUnit::Asic do
  let(:asic) do
    ComputeUnit::Asic.new('/sys/bus/pci/devices/0000:0f:00.0', index: 0)
  end

  it do
    expect(asic).to be_a ComputeUnit::Asic
  end

  it '#find_all' do
    expect(ComputeUnit::Asic.find_all).to be_a Array
  end

  it '#device_class_name' do
    expect(asic.device_class_name).to eq('Asic')
  end
end
