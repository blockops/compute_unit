# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'compute_unit/version'

Gem::Specification.new do |spec|
  spec.name          = 'compute_unit'
  spec.version       = ComputeUnit::VERSION
  spec.authors       = ['Corey Osman']
  spec.email         = ['opselite@blockops.party']
  spec.required_ruby_version = '>= 2.5'
  spec.summary       = 'A ruby library for compute unit devices'
  spec.description   = <<~EOF

    A ruby library that searches the linux sysfs file system for compute unit devices such as
    CPUS, GPUs and other ASIC compute devices. Allows programmatic access to collect real time metrics from the kernel or relatated driver toolchain.
    Is meant to be used as a toolchain for future tools. This library also makes use of opencl library and requires
    the opencl_ruby_ffi gem.

  EOF

  spec.homepage      = 'https://gitlab.com/blockops/compute_unit'
  spec.license       = 'MIT'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = spec.homepage
  spec.metadata['changelog_uri'] = "#{spec.homepage}/-/blob/master/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']
  spec.add_dependency 'sys-proctable', '~> 1.2', '>= 1.2.0'
  spec.add_dependency 'opencl_ruby_ffi', '~> 1.3.4'
  spec.add_development_dependency 'bundler', '~> 2.1'
  spec.add_development_dependency 'rake', '~> 13'
  spec.add_development_dependency 'rspec', '~> 3.0'
end
